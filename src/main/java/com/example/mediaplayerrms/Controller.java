package com.example.mediaplayerrms;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.stage.*;
import javafx.util.Duration;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.Callable;

public class Controller implements Initializable {
    public static final String PLAY_PNG = "src/resources/play.png";
    public static final String PAUSE_PNG = "src/resources/pause.png";
    public static final String RESTART_PNG = "src/resources/restart.png";
    public static final String INIT_MEDIA = "src/resources/drivers_license.wav";
    public static final String VOLUME_PNG = "src/resources/volume.png";
    public static final String FILE_PNG = "src/resources/file.png";
    public static final String NEXT_PNG = "src/resources/next.png";
    public static final String PREV_PNG = "src/resources/prev.png";
    public static final String MUTE_PNG = "src/resources/mute.png";
    public static final String FULLSCREEN_PNG = "src/resources/fullscreen.png";
    public static final String EXIT_PNG = "src/resources/exit.png";
    @FXML
    public VBox vBoxMain;

    @FXML
    public MediaView mediaView;
    public MediaPlayer mediaPlayerMedia;
    public Media myMedia;
    @FXML
    public HBox hBoxControls;

    @FXML
    public HBox hBoxVolume;

    @FXML
    public Button buttonPlay;

    @FXML
    public Label labelCurrentTime;

    @FXML
    public Label labelTotalTime;

    @FXML
    public Label labelFullScreen;

    @FXML
    public Label labelOpenFile;

    @FXML
    public Label labelPrev;

    @FXML
    public Label labelNext;

    @FXML
    public Label labelVolume;

    @FXML
    public Slider sliderVolume;

    @FXML
    public Slider sliderTime;

    public boolean endMedia;
    public boolean isPlaying;
    public boolean isMuted;

    public ImageView imgPlay;
    public ImageView imgPause;
    public ImageView imgRestart;
    public ImageView imgVolume;
    public ImageView imgFullScreen;
    public ImageView imgMute;
    public ImageView imgExit;
    public ImageView imgPrev;
    public ImageView imgNext;
    public ImageView imgFile;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        int iconSize = 48;

        myMedia = new Media(new File(INIT_MEDIA).toURI().toString());
        mediaPlayerMedia = new MediaPlayer(myMedia);
        mediaView.setMediaPlayer(mediaPlayerMedia);

        Image imagePlay = new Image(new File(PLAY_PNG).toURI().toString());
        imgPlay = new ImageView(imagePlay);
        imgPlay.setFitHeight(iconSize);
        imgPlay.setFitWidth(iconSize);

        Image imagePause = new Image(new File(PAUSE_PNG).toURI().toString());
        imgPause = new ImageView(imagePause);
        imgPause.setFitHeight(iconSize);
        imgPause.setFitWidth(iconSize);

        Image imageRestart = new Image(new File(RESTART_PNG).toURI().toString());
        imgRestart = new ImageView(imageRestart);
        imgRestart.setFitHeight(iconSize);
        imgRestart.setFitWidth(iconSize);

        Image imageVolume = new Image(new File(VOLUME_PNG).toURI().toString());
        imgVolume = new ImageView(imageVolume);
        imgVolume.setFitHeight(iconSize);
        imgVolume.setFitWidth(iconSize);

        Image imageFullscreen = new Image(new File(FULLSCREEN_PNG).toURI().toString());
        imgFullScreen = new ImageView(imageFullscreen);
        imgFullScreen.setFitHeight(iconSize);
        imgFullScreen.setFitWidth(iconSize);

        Image imageMute = new Image(new File(MUTE_PNG).toURI().toString());
        imgMute = new ImageView(imageMute);
        imgMute.setFitHeight(iconSize);
        imgMute.setFitWidth(iconSize);

        Image imageExit = new Image(new File(EXIT_PNG).toURI().toString());
        imgExit = new ImageView(imageExit);
        imgExit.setFitHeight(iconSize);
        imgExit.setFitWidth(iconSize);

        Image imagePrev = new Image(new File(PREV_PNG).toURI().toString());
        imgPrev = new ImageView(imagePrev);
        imgPrev.setFitHeight(iconSize);
        imgPrev.setFitWidth(iconSize);

        Image imageNext = new Image(new File(NEXT_PNG).toURI().toString());
        imgNext = new ImageView(imageNext);
        imgNext.setFitHeight(iconSize);
        imgNext.setFitWidth(iconSize);

        Image imageFile = new Image(new File(FILE_PNG).toURI().toString());
        imgFile = new ImageView(imageFile);
        imgFile.setFitHeight(iconSize);
        imgFile.setFitWidth(iconSize);

        buttonPlay.setGraphic(imgPause);
        labelVolume.setGraphic(imgMute);
        labelFullScreen.setGraphic(imgFullScreen);
        labelPrev.setGraphic(imgPrev);
        labelNext.setGraphic(imgNext);
        mediaPlayerMedia.play();
        labelOpenFile.setGraphic(imgFile);

        labelOpenFile.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                FileChooser fileChooser = new FileChooser();
                File file = fileChooser.showOpenDialog(null);
                if (file != null){
                    mediaPlayerMedia.stop();
                    Media media = new Media(file.toURI().toString());
                    mediaPlayerMedia = new MediaPlayer(media);
                    mediaView.setMediaPlayer(mediaPlayerMedia);
                    mediaPlayerMedia.play();

                    mediaPlayerMedia.setOnReady(new Runnable() {
                        @Override
                        public void run() {
                            labelTotalTime.setText(getTime(mediaPlayerMedia.getTotalDuration()));
                            bindCurrentTimeLabel();
                            buttonPlay.setGraphic(imgPause);
                            sliderTime.setMax(mediaPlayerMedia.getTotalDuration().toSeconds());
                        }
                    });
                    mediaPlayerMedia.currentTimeProperty().addListener((observableValue, duration, t1) -> {
                        bindCurrentTimeLabel();
                        if (!sliderTime.isValueChanging()) {
                            sliderTime.setValue(t1.toSeconds());
                        }
                        labelMatchEndVideo(labelCurrentTime.getText(),labelTotalTime.getText());
                    });
                }
            }
        });

        buttonPlay.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                Button buttonPlay = (Button) actionEvent.getSource();
                bindCurrentTimeLabel();

                if (endMedia) {
                    sliderTime.setValue(0);
                    endMedia = false;
                    isPlaying = false;
                }

                if (isPlaying) {
                    buttonPlay.setGraphic(imgPlay);
                    mediaPlayerMedia.pause();
                    isPlaying = false;
                } else {
                    buttonPlay.setGraphic(imgPause);
                    mediaPlayerMedia.play();
                    isPlaying=true;
                }

            }
        });

        hBoxVolume.getChildren().remove(sliderVolume);

        mediaPlayerMedia.volumeProperty().bindBidirectional(sliderVolume.valueProperty());

        bindCurrentTimeLabel();

        sliderVolume.valueProperty().addListener(new InvalidationListener() {
            @Override
            public void invalidated(Observable observable) {
                mediaPlayerMedia.setVolume(sliderVolume.getValue());
                if (mediaPlayerMedia.getVolume() != 0.0) {
                    labelVolume.setGraphic(imgVolume);
                    isMuted = false;
                } else {
                    labelVolume.setGraphic(imgMute);
                    isMuted = true;
                }
            }
        });

        labelVolume.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if (isMuted) {
                    labelVolume.setGraphic(imgVolume);
                    sliderVolume.setValue(0.2);
                    isMuted = false;
                } else {
                    labelVolume.setGraphic(imgMute);
                    sliderVolume.setValue(0);
                    isMuted = true;
                }
            }
        });

        labelVolume.setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if (hBoxVolume.lookup("#sliderVolume")==null){
                    hBoxVolume.getChildren().add(sliderVolume);
                    sliderVolume.setValue(mediaPlayerMedia.getVolume());
                }
            }
        });

        hBoxVolume.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                hBoxVolume.getChildren().remove(sliderVolume);
            }
        });

        vBoxMain.sceneProperty().addListener(new ChangeListener<Scene>() {
            @Override
            public void changed(ObservableValue<? extends Scene> observableValue, Scene scene, Scene t1) {
                if (scene==null && t1!=null){
                    mediaView.fitHeightProperty().bind(t1.heightProperty().subtract(hBoxControls.heightProperty().add(20)));

                }
            }
        });

        labelFullScreen.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                Label label = (Label) mouseEvent.getSource();
                Stage stage = (Stage) label.getScene().getWindow();

                if (stage.isFullScreen()){
                    stage.setFullScreen(false);
                    labelFullScreen.setGraphic(imgFullScreen);
                }else{
                    stage.setFullScreen(true);
                    labelFullScreen.setGraphic(imgExit);
                }
                stage.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
                    @Override
                    public void handle(KeyEvent keyEvent) {
                        if (keyEvent.getCode()==KeyCode.ESCAPE){
                            labelFullScreen.setGraphic(imgFullScreen);
                        }
                    }
                });
            }
        });

        mediaPlayerMedia.totalDurationProperty().addListener(new ChangeListener<Duration>() {
            @Override
            public void changed(ObservableValue<? extends Duration> observableValue, Duration duration, Duration t1) {
                bindCurrentTimeLabel();
                sliderTime.setMax(t1.toSeconds());
                labelTotalTime.setText(getTime(t1));

            }
        });

        sliderTime.valueChangingProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observableValue, Boolean aBoolean, Boolean t1) {
                bindCurrentTimeLabel();
                if (!t1){
                    mediaPlayerMedia.seek(Duration.seconds(sliderTime.getValue()));
                }
            }
        });

        sliderTime.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number t1) {
                bindCurrentTimeLabel();
                double currentTime = mediaPlayerMedia.getCurrentTime().toSeconds();
                if (Math.abs(currentTime-t1.doubleValue())>0.5){
                    mediaPlayerMedia.seek(Duration.seconds(t1.doubleValue()));
                }
                labelMatchEndVideo(labelCurrentTime.getText(),labelTotalTime.getText());
            }
        });

        mediaPlayerMedia.currentTimeProperty().addListener(new ChangeListener<Duration>() {
            @Override
            public void changed(ObservableValue<? extends Duration> observableValue, Duration duration, Duration t1) {
                bindCurrentTimeLabel();
                if (!sliderTime.isValueChanging()){
                    sliderTime.setValue(t1.toSeconds());
                }
                labelMatchEndVideo(labelCurrentTime.getText(),labelTotalTime.getText());
            }
        });

        mediaPlayerMedia.setOnEndOfMedia(new Runnable() {
            @Override
            public void run() {
                buttonPlay.setGraphic(imgRestart);
                endMedia =true;
                if (!labelCurrentTime.textProperty().equals(labelTotalTime.textProperty())){
                    labelCurrentTime.textProperty().unbind();
                    labelCurrentTime.setText(getTime(mediaPlayerMedia.getTotalDuration()) + " / ");
                }
            }
        });

        labelNext.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                mediaPlayerMedia.seek(mediaPlayerMedia.getCurrentTime().add(Duration.seconds(5)));

            }
        });

        labelPrev.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                mediaPlayerMedia.seek(mediaPlayerMedia.getCurrentTime().subtract(Duration.seconds(5)));

            }
        });
    }

    public void bindCurrentTimeLabel(){
        labelCurrentTime.textProperty().bind(Bindings.createStringBinding(new Callable<String>() {
            @Override
            public String call() throws Exception {
                return getTime(mediaPlayerMedia.getCurrentTime()) + " / ";
            }
        }, mediaPlayerMedia.currentTimeProperty()));
    }

    public String getTime(Duration time){
        int hours = (int)time.toHours();
        int minutes = (int) time.toMinutes();
        int seconds = (int) time.toSeconds();

        if (seconds > 59)
            seconds = seconds % 60;
        if (minutes > 59)
            seconds = seconds % 60;
        if (hours > 59)
            seconds=seconds % 60;

        return (hours > 0) ?
                String.format("%d:%02d:%02d",hours,minutes,seconds) :
                String.format("%02d:%02d",minutes,seconds);
    }
    public void labelMatchEndVideo(String labelTime, String labelTotalTime){
        for (int i = 0; i < labelTotalTime.length(); i++) {
            if (labelTime.charAt(i) != labelTotalTime.charAt(i)) {
                endMedia = false;
                if (isPlaying) {
                    buttonPlay.setGraphic(imgPause);
                } else {
                    buttonPlay.setGraphic(imgPlay);
                }
                break;
            } else{
                endMedia = true;
                buttonPlay.setGraphic(imgRestart);
            }
        }
    }


}

